#!/bin/bash

# Do not use with current (Helios) template!

title="guitarra libre"
while true; do case $1 in
"-t"|"--title") title="$2"; shift 2;;
"--") shift; break;;
*) break;;
esac; done

for f in $@; do
	if [ ! -f $f ]; then
		echo "$f: not found"
		continue
	fi
	echo "Compiling $f"
	o=${f/.md/.html}
	cat header.html | sed -e "s/%TITLE%/$title/" >.header.swp
	pandoc -Ss -H .header.swp -B body.html -A footer.html -o $o $f
	job=1
done

if [ $job ]; then echo "Done."; else echo "Nothing to do."; fi
